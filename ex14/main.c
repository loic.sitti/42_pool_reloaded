/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 21:59:47 by loic              #+#    #+#             */
/*   Updated: 2019/01/07 22:20:34 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int				ft_sqrt(int nb);

int main()
{
	int nb;
	int	i;
	
	i = 1;
	printf("Enter an int to find his square root: \n");
	scanf("%d", &nb);

	if (nb < 0)
		printf("Error! the number must be > 0 !\n");
	if (ft_sqrt(nb) == 0)
		printf("This number doesn't have a square root!\n");
	else
	{
		printf("The square root is %d", ft_sqrt(nb));
	}
	return (0);
}
