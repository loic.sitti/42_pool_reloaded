/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_file.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/02/17 13:34:31 by loic              #+#    #+#             */
/*   Updated: 2019/02/17 14:38:14 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */


#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include "../includes/ft.h"
#define BUF_SIZE 4096

void	ft_display_file(char *filename)
{
	int	fd;
	int	read_ret;
	char	buf[BUF_SIZE];

	fd = open(filename, O_RDONLY);
	if (fd == -1)
			return ;
	while ((read_ret = read(fd, buf, BUF_SIZE)) > 0)
			write(1, buf, read_ret);
	close(fd);

}

int main(int ac, char **av)
{
	if (ac == 1)
			write (2, "file name missing.\n", 19);
	if (ac > 2)
			write (2, "Too many arguments.\n", 20);
	if (ac > 2 || ac == 1)
		return (1);
	ft_display_file(av[1]);
	return 0;
}
