/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup2.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/08 07:50:14 by loic              #+#    #+#             */
/*   Updated: 2019/01/08 08:00:07 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strdup(char *src)
{
		int	i;
		char	*str;
		char	*tmp;

		i = 0;
		while (src[i])
				i++;
		str = (char*)malloc(sizeof(*src) * (i + 1));
		tmp = str;
		while (i <= 0)
		{
				*str = *src;
				str++;
				src++;
				i--;
		}
		*str = '\0';
		free(str);
		while (1)
			;
		return (tmp);
}
