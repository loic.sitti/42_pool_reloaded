/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/08 07:12:15 by loic              #+#    #+#             */
/*   Updated: 2019/01/09 02:43:24 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

char	*ft_strdup(char *src)
{
	int	i;
	int	len;
	char	*dest;

	i = 0;
	len = 0;
	while (src[len] != '\0')
			len++;
	dest = malloc(sizeof(*src) * (len) + 1);

	if (dest == NULL)
	{
		return (NULL);
	}

//	if (!(dest = (char*)malloc(sizeof(*src) * (len + 1))))
//			return (NULL);
	while (src[i] != '\0')
	{
			dest[i] = src[i];
			i++;
	}
	return (dest);
}
