/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/08 07:33:22 by loic              #+#    #+#             */
/*   Updated: 2019/01/09 02:45:24 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>
#include <stdlib.h>

char	*ft_strdup(char *src);

int main()
{
	char *str = "testthisstrdupdemerde";
	char *ptr;
	ptr = ft_strdup(str);
	printf("String = %s\n", str);
	printf("ptr = %s", str);
	return (0);
}
