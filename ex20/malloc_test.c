/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc_test.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/08 17:28:03 by loic              #+#    #+#             */
/*   Updated: 2019/01/08 18:15:03 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>
#define	LEN		42

int main()
{
	int	i;
	char	*str;

	str = (char*)malloc(sizeof(*str) * (LEN + 1));
	i = 0;
	while (i < LEN)
	{
		str[i] = '0' + (i % 10);
		i++;
	}
	str[i] = '\0';

	/* Initial memory allocation */
//	printf("String = %s\n Adress = %p\n", str, str);
	/* Free memory */

	free(str);
	while (1)
		;
//	printf("String = %s\n Adress = %p\n", str, str);

	return(0);
}
