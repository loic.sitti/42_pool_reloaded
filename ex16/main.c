/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/08 00:15:18 by loic              #+#    #+#             */
/*   Updated: 2019/01/08 00:21:48 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int				ft_strlen(char *str);

int main()
{
	char *str = "Pierrot le fou";
	int	i = 0;
	printf("%s\n", &(str[i]));
	printf("The lenght of the string is %d", ft_strlen(str));

	return (0);
}
