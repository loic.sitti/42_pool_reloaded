/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/08 18:21:24 by loic              #+#    #+#             */
/*   Updated: 2019/01/08 19:53:35 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int				*ft_range(int min, int max);

int main()
{
	int* res;
	int	i;

	res = ft_range(2, 10);
	for (i = 0; i < 8; i++)
			printf("%d,", res[i]);
	printf("\n");

	res = ft_range(10, 5);
	printf("%x\n", (unsigned int)res);
// "%x, hex format specifier"
	return (0);
}
