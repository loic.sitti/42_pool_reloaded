/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range2.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/08 19:25:43 by loic              #+#    #+#             */
/*   Updated: 2019/01/08 19:54:09 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int				*ft_range(int min, int max)
{
	int	*str;
	int	*tmp;

	if (min >= max)
			return(0);
	str = (int*)malloc(sizeof(*str) * (max - min + 1));
	tmp = str;
	while (max > min)
	{
			*str = min;
			str++;
			min++;
	}
	*str = 0;
	return (tmp);
}
