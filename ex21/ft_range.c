/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/08 18:17:37 by loic              #+#    #+#             */
/*   Updated: 2019/01/08 19:25:34 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int				*ft_range(int min, int max)
{
	int	*tab;
	int	i;
	int	len;

	if (min >= max)
			return (NULL);

	i = 0;
	len = max - min;
	tab = (int*)malloc(sizeof(min) * (len));

//	if (!(tab = (int*)malloc(sizeof(min) * (len))))
//	{
//			return (NULL);
//	}
	while (i < len)
	{
			tab[i] = min + i;
			i++;
	}
	return (tab);
}
