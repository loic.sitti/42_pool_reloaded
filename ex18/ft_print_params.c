/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_params.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/08 01:44:38 by loic              #+#    #+#             */
/*   Updated: 2019/01/08 01:57:31 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int			ft_putchar(char c);

int main(int argc, char **argv)
{
	int	i;
	int	j;

	i  = 1;
	while (i <argc)
	{
			j = 0;
			while (argv[i][j])
			{
					ft_putchar(argv[i][j]);
					j++;
			}
			ft_putchar('\n');
			i++;
	}
	return 0;
}
