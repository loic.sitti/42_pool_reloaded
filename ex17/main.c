/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/08 00:29:36 by loic              #+#    #+#             */
/*   Updated: 2019/01/08 01:37:22 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int				ft_strcmp(char *s1, char *s2);

int main()
{
	int ret;
	char *s1 = "Cowboy";
	char *s2 = "Bebop";
	ret = ft_strcmp(s1, s2);

	if (ret < 0)
		printf("s1 is less than s2");
	else if (ret > 0)
	{
		printf("s1 is more than s2");
	}
	else
		printf("s1 is eaual to s2");
}
