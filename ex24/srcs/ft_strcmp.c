/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/16 13:59:38 by loic              #+#    #+#             */
/*   Updated: 2019/02/16 21:57:24 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

/*
 * ft_strcmp takes in two char[], s1 and s2
 * set up an unsigned char for the values, s1v and s2v
 * set up an iterator i and set it to 0
 * while s1[i] is the same as s2[i] and s1[i] isn't null
 *	move down the string, by iterating i
 * set s1v to s1[i];
 * set s2v s2[i];
 * return the difference of s1v and s2v: s1v - s2v
 */

int			ft_strcmp(char *s1, char *s2)
{
	unsigned char	s1_value;
	unsigned char	s2_value;
	int	i;

	i = 0;
	while (s1[i] == s2[i] && s1[i] != 0)
			i++;
	s1_value = s1[i];
	s2_value = s2[i];
	return (s1_value - s2_value);
}
