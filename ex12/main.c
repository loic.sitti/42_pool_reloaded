/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 18:12:58 by loic              #+#    #+#             */
/*   Updated: 2019/01/07 19:00:14 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int			ft_iterative_factorial(int nb);

int main()
{
	int	nb;
	int	i;
	
	i = 1;
	printf("Enter an interger to find his facto: \n");

	scanf("%d", &nb);
	if (nb < 0)
		printf("Error! Factorial of negative number doesn't exist.");
	else
	{
		printf("His factorial is %d\n", ft_iterative_factorial(nb));
	}
	return (0);
}
