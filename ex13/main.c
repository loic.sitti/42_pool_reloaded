/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 21:48:50 by loic              #+#    #+#             */
/*   Updated: 2019/01/07 21:59:14 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int			ft_recursive_factorial(int nb);

int main()
{
	int	nb;
	printf("Enter an int to find his fatorial:\n");
	scanf("%d", &nb);

	if (nb < 0)
		printf("Error! the number must be > 0 !");
	else
	{
		printf("The factorial is %d", ft_recursive_factorial(nb));
	}
	return(0);
}
