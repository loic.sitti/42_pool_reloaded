/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: loic <marvin@42.fr>                        +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/07 22:22:01 by loic              #+#    #+#             */
/*   Updated: 2019/01/08 00:15:08 by loic             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

void	ft_putstr(char *str);

int main ()
{
	char *str = "Bouwa!";
	int	i;
	
	i = 0;
	ft_putstr(str);
	printf("\n");
	printf("%p is the address of str[0]", &(str[i]));
	return (0);
}
